#! /usr/bin/env sh

# Maintainer: shivanandvp <shivanandvp.oss@gmail.com>
pkgname=archlinux-driver-manager
pkgver=0.0.1
pkgrel=1
arch=('x86_64')
pkgdesc="A driver manager for Arch Linux"
license=('MPL-2.0')
depends=('pacman')
makedepends=('git'
             'rustup'
             'rust')
provides=("${pkgname}")
conflicts=("${pkgname}")
replaces=()
backup=()
options=()
install=
source=()
noextract=()
md5sums=()

instdir="/opt/ArchLinuxDriverManager"

PROJECT_DIRECTORY="$(dirname -- "$(dirname -- "$(pwd)")")"
RESOURCE_DIRECTORY="$PROJECT_DIRECTORY/scripts/packaging"
NUMBER_OF_PROCESSORS="$(nproc)"

build() {
    # Starts in the `src` directory
    RUSTUP_TOOLCHAIN=stable \
    cargo build \
        --release \
        --locked \
        --all-features \
        --target-dir="target"
}

check() {
    # Starts in the `src` directory
    RUSTUP_TOOLCHAIN=stable \
    cargo test \
        --release \
        --locked \
        --all-features \
        --target-dir="target"
}

package() {
    # Starts in the `src` directory

    # Copy the Database file
    (
        cd "$PROJECT_DIRECTORY" && \
        install -d -m 755 "${pkgdir}/${instdir}"
        install -m 644 "database.ron" "${pkgdir}/${instdir}/" 
    )

    # Copy the binaries
    install -d -m 755 "${pkgdir}/${instdir}/bin" # Temporary directory to store the binaries
    install -d -m 755 "${pkgdir}/usr/bin" # Create path to store the symbolic links to binaries 

    find "target/release" \
     -maxdepth 1 \
     -executable \
     -type f \
     \( ! -iname "*.d" -and ! -iname "*.rlib" -and ! -iname "*.cargo-lock" \) \
     -exec install -m 755 "{}" "${pkgdir}/${instdir}/bin" \; # Find executable binaries in the release directory and store it in the temporary bin directory so that they can be temporarily handled separately from other files in the application base directory

    for _executable in "${pkgdir}/${instdir}"/bin/*; do # For each collected binary file in the temporary bin directory
        _filename=$(basename "$_executable") # Get the filename from the path 
        mv "$_executable" "${pkgdir}/${instdir}/" # Move binary files back to the application directory
        ln -s "${instdir}/${_filename}" "${pkgdir}/usr/bin/${_filename}" # Created symbolic links to the application binaries in the /usr/bin directory
    done      
    rmdir "${pkgdir}/${instdir}/bin" # Remove the temporary binary directory    

    # Create a symlink for "driver-manager" to call "archlinux-driver-manager"
    ln -s "/usr/bin/archlinux-driver-manager" "${pkgdir}/usr/bin/driver-manager"
    chmod 755 "${pkgdir}/usr/bin/driver-manager"

    # Copy the .desktop entry to the appropriate location with the correct permissions
    # install -D -m 644 "$RESOURCE_DIRECTORY/${pkgname}.desktop" "${pkgdir}/usr/share/applications/${pkgname}.desktop"

    # Copy the License and Readme files
    (
        cd "$PROJECT_DIRECTORY" && \
        install -m 644 LICENSE README.md "${pkgdir}/${instdir}/" 
    )  
}